# scripts

All the javascript was copy/pasted into a file and uploaded to GitHub, and then
the [GitCDN.xyz](gitcdn.xyz) service was used. I would have uploaded it to GitLab,
but my (brief) attempts at getting the code to work from there failed (strict type
checking).

I didn't upload it to GitHub to steal traffic from the original authors, I needed
the code to be on HTTPS websites so that it wouldn't throw errors in Google Chrome.

As you can also see, the Asteroids game (Kickass) and X-Ray Goggles are both loaded
from their original websites because their original websites have HTTPS.

Please go to [https://github.com/turquoise-turtle/reimagined-giggle](https://github.com/turquoise-turtle/reimagined-giggle)
to see some of the files. Please note the repository name was a suggestion from GitHub.