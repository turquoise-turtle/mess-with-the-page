# Mess With The Page

A chrome extension that has a few bookmarklets for changing the DOM of the current page

Notes: 

* [a few of the bookmarklets](js/README.md)) were copy/pasted into a GitHub repo (strict type checking errors with GitLab)
and then loaded over GitCDN.xyz so that they would work through HTTPS. I didn't modify the files, but you can check
[the GitHub repo](https://github.com/turquoise-turtle/reimagined-giggle) if you want.
* The stuff.js and main.css files are minified and then put in the main.js file as innerHTML of `<script>` and `<style>` tags respectively.(I
haven't been able to work out how to use Chrome's message passing and executeScript and insertCSS to load the scripts and CSS better)

## Install:

1. Download the [latest zip](https://gitlab.com/turquoise-turtle/mess-with-the-page/repository/master/archive.zip)
2. Unzip the folder
3. Go to chrome://extensions
4. Check Developer Mode
5. Click Load Unpacked Extension
6. Choose the root folder of the unzipped extension

## Use it:

1. Click the blue paint mess extension icon
2. Click one of the actions

## [Contributing](contributing.md)

