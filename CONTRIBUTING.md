# Contributing

Hi! Thanks for being willing to contribute.

If you want to add new bookmarklet/HTML/CSS/JS or things that mess with the page
(alter the DOM in fun ways), submit a pull request to this repo and the GitHub one
(see /js/README.md) with the full files and the [insertnamehere]activate(); function in 
js/stuff.js.

Remember it's GPLv3 so anybody who shares any part of the project has to share
the full source as well.

The following list will be updated every time someone else contributes and the
changelog will show what has been changed:

### Contributors:

* turquoise-turtle